<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 17:32
 */

namespace ETWS\Framework;

use yii\base\Object;

class Registrar extends Object
{
    const MODULE = 'module';

    private static $paths = [];

    public static function register($type, $name, $path)
    {
        self::$paths[$type][$name] = str_replace('\\', '/', $path);
    }

    public function getPaths($type = self::MODULE)
    {
        return self::$paths[$type];
    }
}