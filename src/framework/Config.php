<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 17:16
 */

namespace ETWS\Framework;

use ETWS\Framework\Config\Module;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class Config extends Component
{
    protected $area;
    protected $config = [];

    /**
     * @var Module
     */
    protected $moduleConfig;

    protected $areaList = [
        'default',
        'console',
        'backend',
        'frontend'
    ];

    protected $defaultConfig = [
        'default' => [
            'id' => 'app',
            'basePath' => BP,
        ],
        'console' => [],
        'backend' => [],
        'frontend' => []
    ];

    /**
     * Config constructor.
     * @param Module $module
     * @param array $config
     */
    public function __construct(
        Module $module,
        $config = []
    )
    {
        $this->moduleConfig = $module;
        parent::__construct($config);
    }


    /**
     * @param $area
     * @return $this
     */
    public function load($area)
    {
        $this->area = $area;
        $moduleList = $this->moduleConfig->getList();

        $config = ArrayHelper::merge(
            $this->getDefaultConfig(),
            $this->getLocalConfig()
        );
        foreach ($moduleList as $module) {
            $config = ArrayHelper::merge($config, $this->getModuleConfig($module));
        }
        $this->config['default'] = $config['default'];
        foreach ($this->areaList as $area) {
            if ($area != 'default') {
                $this->config[$area] = ArrayHelper::merge($config['default'], $config[$area]);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getDefaultConfig()
    {
        return $this->defaultConfig;
    }

    /**
     * @return array
     */
    protected function getLocalConfig()
    {
        $filePath = BP . '/app/config/local.php';
        $config = [];
        if (is_file($filePath)) {
            $config = include $filePath;
        }
        return is_array($config) ? $config : [];
    }

    protected function getModuleConfig(\stdClass $module)
    {
        $nameSpace = str_replace('_', '\\', $module->name);
        $moduleData = [
            'default' => [],
            'console' => [],
            'backend' => [
                'modules' => [
                    $module->name => [
                        'class' => $nameSpace . '\Module',
                        'controllerNamespace' => $nameSpace . '\Controllers'
                    ],
                ],
            ],
            'frontend' => [
                'modules' => [
                    $module->name => [
                        'class' => $nameSpace . '\Module',
                        'controllerNamespace' => $nameSpace . '\Controllers'
                    ],
                ],
            ]
        ];

        $config = ArrayHelper::merge(
            $moduleData,
            $this->getModuleAppConfig($module),
            $this->getModuleComponentsConfig($module)
        );

        return $config;
    }

    protected function getModuleAppConfig($module)
    {
        return $this->getModuleConfigByNameFromFile('app', $module);
    }

    protected function getModuleConfigByNameFromFile($name, $module, $callback = [])
    {
        $config = [];
        foreach ($this->areaList as $area) {
            $areaPath = $area . '/';
            if ($areaPath == 'default/') {
                $areaPath = '';
            }
            $pattern = $module->path . '/etc/' . $areaPath . $name . '.{php,xml}';
            $data = [];
            foreach (glob($pattern, GLOB_BRACE) as $item) {
                $pathinfo = pathinfo($item);
                switch ($pathinfo['extension']) {
                    case 'php':
                        if (array_key_exists('php', $callback)) {
                            $data = call_user_func($callback['php'], $item);
                        } else {
                            $data = include $item;
                            $data = is_array($data) ? $data : [];
                        }
                        break;
                    case 'xml':
                        if (array_key_exists('php', $callback)) {
                            $data = call_user_func($callback['php'], $item);
                        } else {
                            throw new \yii\base\InvalidCallException("Нету callback для обработки '$item'");
                        }
                        break;
                }
            }
            $config[$area] = $data;
        }
        return $config;
    }

    protected function getModuleComponentsConfig($module)
    {
        return $this->getModuleConfigByNameFromFile('components', $module, [
            'php' => function ($item) {
                $tmp = include $item;
                $data['components'] = is_array($tmp) ? $tmp : [];
                return $data;
            }
        ]);
    }

    /**
     * @return array
     */
    public function get()
    {
        if (isset($this->config[$this->area])) {
            return $this->config[$this->area];
        } else {
            return $this->config['default'];
        }
    }
}

\Yii::setAlias('@ETWS/Framework', __DIR__);