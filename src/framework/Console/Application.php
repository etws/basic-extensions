<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 18.03.2017
 * Time: 13:50
 */

namespace ETWS\Framework\Console;

class Application extends \yii\console\Application {
    public $id = 'app';

    public function __construct(array $config = [])
    {
        $config = [
            'id' => 'app',
            'basePath' => BP,
        ];
        parent::__construct($config);
    }


}