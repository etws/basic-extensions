<?php

namespace ETWS\Framework\Helpers;

use ETWS\Framework\Helpers\Image\Options;
use yii\base\InvalidParamException;
use yii\helpers\FileHelper;
use yii\imagine\Image as ImageBase;

class Image
{
    public $mediaPath = '@webroot/media/';
    public $cachePath = '@webroot/media/_cache/';

    protected $_imagePath;
    protected $_thumbnailPath;
    
    /**
     * @param $image
     * @param Options $options
     * @return mixed|string
     */
    public function thumbnail($image, Options $options)
    {
        if (!$options->validate()) {
            throw new InvalidParamException('Check options. ' . print_r($options->errors, true));
        }
        $this->init();

        $path = \Yii::getAlias($image);
        $this->_thumbnailPath = $this->createCachePath($path, $options);

        if (!is_file($this->_thumbnailPath)) {
            ImageBase::thumbnail($image, $options->width, $options->height, $options->thumbnailMode)->save($this->_thumbnailPath);
        }

        $webPath = str_replace($this->mediaPath, '', $this->_thumbnailPath);
        $webPath = \Yii::getAlias('@web/media/') . $webPath;
        return $webPath;
    }

    public function init()
    {
        $this->mediaPath = \Yii::getAlias($this->mediaPath);
        $this->cachePath = \Yii::getAlias($this->cachePath);
    }

    protected function createCachePath($path, Options $options)
    {
        $pathInfo = pathinfo($path);
        $part = str_replace($this->mediaPath, '', $pathInfo['dirname']);
        $hash = $this->hash($options);
        $newDirname = $this->cachePath . $options->width . 'x' . $options->height . '_' . $hash . '/' . $part;

        FileHelper::createDirectory($newDirname, 0775, true);

        return $newDirname . '/' . $pathInfo['basename'];
    }

    /**
     * Generate a CRC32 hash for the directory path. Collisions are higher
     * than MD5 but generates a much smaller hash string.
     * @param Options $options
     * @return string
     */
    protected function hash(Options $options)
    {
        $options = json_encode($options->getAttributes());
        return sprintf('%x', crc32($options . \Yii::getVersion()));
    }
}