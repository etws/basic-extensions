<?php

namespace ETWS\Framework\Helpers\Image;

use yii\base\Model;

class Options extends Model
{
    public $width;
    public $height;
    public $thumbnailMode;

    public function rules()
    {
        return [
            [['width'], 'required'],
            [['width', 'height'], 'integer', 'min' => '1'],
            ['height', 'default', 'value' => function ($model) {
                return $model->width;
            }],
            ['thumbnailMode', 'in', 'range' => [
                \Imagine\Image\ManipulatorInterface::THUMBNAIL_INSET,
                \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
            ]],
            ['thumbnailMode', 'default', 'value' => \Imagine\Image\ManipulatorInterface::THUMBNAIL_INSET],
        ];
    }
}