<?php
namespace ETWS\Framework\Widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu as YiiMenu;

class Menu extends YiiMenu
{
    /** @var string */
    public $linkTemplate = '<a href="{url}"{options}>{label}</a>';

    /** @var string  */
    public $labelTemplate = '<span>{label}</span>';

    /**
     * @inheritdoc
     */
    protected function renderItem($item)
    {
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{options}' => $this->renderLinkOptions($item),
                '{label}' => $item['label'],
            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
                '{options}' => $this->renderLinkOptions($item),
            ]);
        }
    }

    protected function renderLinkOptions($item)
    {
        if (!array_key_exists('linkOptions', $item)){
            return '';
        }
        $options = $item['linkOptions'];
        if (empty($options)) {
            return '';
        }
        $string = ' ';
        foreach ($options as $key => $option) {
            $string .= $key . '="' . $option . '"';
        }
        return $string;
    }
}
