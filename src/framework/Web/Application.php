<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 17:27
 */

namespace ETWS\Framework\Web;

class Application extends \yii\web\Application
{
    const AREA_BACKEND = 'backend';
    const AREA_FRONTEND = 'frontend';
    const AREA_CONSOLE = 'console';
    public static $composerAutoload;
    protected $area;

    /**
     * Application constructor.
     * @param \Composer\Autoload\ClassLoader $autoload
     */
    public function __construct($autoload)
    {
        self::$composerAutoload = $autoload;

        /** @var \ETWS\Framework\Config $config */
        $config = \Yii::$container->get('ETWS\Framework\Config');

        $configArray = $config->load($this->getArea())->get();

        parent::__construct($configArray);
    }

    public function getArea()
    {
        if (!isset($this->area)) {
            $this->area = $this->detectArea();
        }
        return $this->area;
    }

    protected function detectArea()
    {
        //todo: proverka na web zapros
        if (false) {
            return self::AREA_CONSOLE;
        }

        /** @var \ETWS\Framework\Web\Request $request */
        $request = \Yii::$container->get('ETWS\Framework\Web\Request');

        //todo: vzjatj iz configa base_url i sravnitj s $request->getHostInfo();
        if (true) {
            $frontName = explode('/', $request->getPathInfo());
            $frontName = $frontName[0];
            //todo: bratj backend frontName iz konfiga
            if ($frontName === 'admin') {
                return self::AREA_BACKEND;
            }
        }

        return self::AREA_FRONTEND;
    }

    public function coreComponents()
    {
        return array_merge(parent::coreComponents(), [
            'request' => ['class' => 'ETWS\Framework\Web\Request'],
        ]);
    }
}