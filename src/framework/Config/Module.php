<?php

namespace ETWS\Framework\Config;

use ETWS\Framework\Registrar;
use MJS\TopSort\Implementations\StringSort;

class Module
{
    /** @var  \stdClass[] */
    protected $moduleList;

    /**
     * Возвращает отсортированый список модулей.
     *
     * @return \stdClass[]
     */
    public function getList()
    {
        if (!$this->moduleList) {
            $this->loadModules()->sortModules();
        }
        return $this->moduleList;
    }

    /**
     * Сортировка списка модулей используя зависимости.
     *
     * @return $this
     */
    protected function sortModules()
    {
        $sorter = new StringSort();
        foreach ($this->moduleList as $moduleName => $data) {
            if (property_exists($data, 'sequence') && !empty($data->sequence)) {
                $sorter->add($moduleName, $data->sequence);
            } else {
                $sorter->add($moduleName);
            }
        }
        $moduleList = [];
        foreach ($sorter->sort() as $module) {
            $moduleList[$module] = $this->moduleList[$module];
        }
        $this->moduleList = $moduleList;

        return $this;
    }

    protected function loadModules()
    {
        $register = new Registrar();
        $registeredModules = $register->getPaths(Registrar::MODULE);
        $moduleList = [];

        $moduleMainEtcFiles = [];
        foreach ($registeredModules as $moduleName => $modulePath) {
            if (is_file($modulePath . '/etc/module.xml')) {
                $moduleMainEtcFiles[$moduleName] = $modulePath . '/etc/module.xml';
            }
        }
        foreach ($moduleMainEtcFiles as $moduleName => $moduleInfoFile) {
            $fileInfo = pathinfo($moduleInfoFile);

            if ($fileInfo['extension'] == 'xml') {
                $file = new XmlConfig($moduleInfoFile);
                if ($file->validate()) {
                    $module = (object)([
                        'path' => $registeredModules[$moduleName]
                    ]);
                    /** @var \DOMElement $node */
                    $node = $file->getConfigNode()->childNodes->item(0);
                    $this->setModuleInfoFromNode($module, $node);
                    $moduleList[$module->name] = $module;
                } else {
                    //TODO: придумать как обозначать ошибку при обработке модуля.
                }
            }
        }
        $this->moduleList = $moduleList;

        return $this;
    }

    /**
     * Заполняем stdClass данными о модуле.
     *
     * @param \stdClass $module
     * @param \DOMElement $node
     */
    protected function setModuleInfoFromNode(\stdClass $module, \DOMElement $node)
    {
        $module->name = $node->getAttribute('name');
        $module->version = $node->getAttribute('version');

        $sequenceNode = $node->getElementsByTagName('sequence');
        if ($sequenceNode->length == 1 && $sequenceNode->item(0)->hasChildNodes()) {
            $sequences = [];
            $sequenceNodes = $sequenceNode->item(0)->getElementsByTagName('module');
            foreach ($sequenceNodes as $sequenceNode) {
                /** @var \DOMElement $sequenceNode */
                $sequences[] = $sequenceNode->getAttribute('name');
            }
            $module->sequence = $sequences;
        }
    }
}