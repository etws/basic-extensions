<?php

namespace ETWS\Framework\Config;

use yii\base\InvalidParamException;

/**
 * Class XmlConfig
 * @package ETWS\Framework\Config
 */
class XmlConfig
{
    const XSD_ATTRIBUTE_NAME = 'xsi:noNamespaceSchemaLocation';

    /**
     * @var string
     */
    protected $filePath;

    /**
     * @var \DOMDocument
     */
    protected $dom;
    /**
     * @var \DOMElement
     */
    protected $configNode;

    /**
     * @var \LibXMLError[]|null
     */
    protected $errors = [];

    /**
     * XmlConfig constructor.
     * @param $filePath string
     */
    public function __construct($filePath)
    {
        if (is_file($filePath)) {
            $this->filePath = $filePath;
            $dom = new \DOMDocument();
            $dom->preserveWhiteSpace = false;
            if (!@$dom->load($filePath)) {
                throw new InvalidParamException("Invalid xml file: '$filePath'");
            }
            $this->dom = $dom;

            $configNodes = $dom->getElementsByTagName('config');

            if ($configNodes->length != 1) {
                throw new InvalidParamException("Invalid xml file: '$filePath', config section can be only one");
            }
            foreach ($configNodes as $configNode) {
                $this->configNode = $configNode;
            }
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $xsd = $this->getXSD();
        if (!$xsd) {
            $this->errors = null;
            return true;
        }

        libxml_use_internal_errors(true);
        if ($this->dom->schemaValidate($xsd)) {
            $return = true;
        } else {
            $errors = libxml_get_errors();
            $this->errors = $errors;
            $return = false;
            /*foreach ($errors as $error) {
                var_dump(get_class($error));
                printf('XML error "%s" [%d] (Code %d) in %s on line %d column %d' . "\n",
                    $error->message, $error->level, $error->code, $error->file,
                    $error->line, $error->column);
            }*/
            libxml_clear_errors();
        }
        libxml_use_internal_errors(false);

        return $return;
    }

    /**
     * @return string|null
     */
    public function getXSD()
    {
        if ($this->configNode->hasAttribute(self::XSD_ATTRIBUTE_NAME)) {
            $xsdAlias = $this->configNode->getAttribute(self::XSD_ATTRIBUTE_NAME);
            $xsdAlias = \Yii::getAlias($xsdAlias);
            return is_file($xsdAlias) ? $xsdAlias : null;
        }
        return null;
    }

    /**
     * @return \LibXMLError[]|null
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function getConfigNode()
    {
        return $this->configNode;
    }
}