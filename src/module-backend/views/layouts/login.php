<?php
/* @var $this \yii\web\View */
/* @var $content string */

use EtYii\backend\assets\BackendAssets;
use yii\helpers\Html;

BackendAssets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="<?= \Yii::$app->assetManager->getPublishedUrl('@EtYii/backend/assets/sources') ?>/ico/favicon.ico">
</head>
<body class="full-lg">
<?php $this->beginBody() ?>
<div id="wrapper">
    <div id="loading-top">
        <div id="canvas_loading"></div>
        <span>Checking...</span>
    </div>
    <div id="main">
        <?= $content ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>