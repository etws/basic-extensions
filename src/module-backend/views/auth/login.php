<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model EtYii\backend\models\user\forms\LoginForm */

use yii\widgets\ActiveForm;

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="account-wall">
                <section class="align-lg-center">
                    <div class="site-logo"></div>
                    <h1 class="login-title"><span>ET</span>Control<small>Documentation Management</small></h1>
                </section>
                <?php $form = ActiveForm::begin([
                    'enableClientScript' => false,
                    'enableClientValidation' => false,
                    'id' => 'form-signin',
                    'options' => ['class' => 'form-signin', 'autocomplete' => 'off'],
                    'fieldConfig' => [
                        'template' => "{input}",
                    ],
                ]); ?>
                <section>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                        <input type="email" id="loginform-email" class="form-control" name="LoginForm[email]" autofocus="" placeholder="Email" autocomplete="off" required>
                        <?//= $form->field($model, 'email')->textInput(['class' => 'form-control', 'autofocus' => true, 'placeholder' => 'Email']) ?>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                        <input type="password" id="loginform-password" class="form-control" name="LoginForm[password]" placeholder="Password" required>
                    <?//= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Password']) ?>
                    </div>
                    <button class="btn btn-lg btn-theme-inverse btn-block" type="submit" id="sign-in">Sign in</button>

                </section>
                <section class="clearfix">
                    <a href="#" class="pull-right help">Forget Password?</a>
                </section>
                <?php ActiveForm::end(); ?>
                <a href="#" class="footer-link">&copy; 2011-<?= date('Y'); ?> ET Web Solutions. All Rights Reserved.</a>
            </div>
        </div>
    </div>
</div>
<?php $jsCode = <<<JS
function toCenter(){var a=jQuery("#main").outerHeight(),b=jQuery(".account-wall").outerHeight(),c=(a-b)/2;c>30?jQuery(".account-wall").css("margin-top",c-15):jQuery(".account-wall").css("margin-top",30)}toCenter();var toResize;jQuery(window).resize(function(a){clearTimeout(toResize),toResize=setTimeout(toCenter(),500)});

//Canvas Loading
var throbber = new Throbber({  size: 32, padding: 17,  strokewidth: 2.8,  lines: 12, rotationspeed: 0, fps: 15 });
throbber.appendTo(document.getElementById('canvas_loading'));
throbber.start();


jQuery("#form-signin").submit(function(event){
    event.preventDefault();
    var main=jQuery("#main");
    //scroll to top
    main.animate({
        scrollTop: 0
    }, 500);
    main.addClass("slideDown");

    // send email and password to php check login
    jQuery.ajax({
        url: jQuery(this).attr('action'), data: jQuery(this).serialize(), type: "POST", dataType: 'json',
        success: function (e) {
            setTimeout(function () { main.removeClass("slideDown") }, !e.status ? 500:900);
            if (!e.status) {
                jQuery.notific8('Check Email or Password again !! ',{ life:5000,horizontalEdge:"top", theme:"danger" ,heading:"ERROR"});
                return false;
            }
            setTimeout(function () { jQuery("#loading-top span").text("Yes, account is access...") }, 300);
            setTimeout(function () { jQuery("#loading-top span").text("Redirect to account page...")  }, 800);
            setTimeout( "window.location.href='" + e.href + "'", 900 );
        }
    });

});
JS;
$this->registerJs($jsCode); ?>
