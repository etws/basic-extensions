<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 20:37
 */

namespace EtYii\backend;

class UrlManager extends \yii\web\UrlManager {

    /**
     * @param \EtYii\web\Request $request
     * @return array|bool
     */
    public function parseRequest($request)
    {
        $pathInfo = $request->getPathInfo();
        $request->setPathInfo(str_replace($request->frontName, '', $pathInfo));
        //exit();
        return parent::parseRequest($request);
    }

    public function createUrl($params) {
        $url = parent::createUrl($params);
        if (\Yii::$app->getRequest()->frontName) {
           $url = '/' . \Yii::$app->getRequest()->frontName . $url;
        }
        return $url;
    }
}