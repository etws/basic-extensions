<?php
\Yii::setAlias('@ETWS/Backend', dirname(__DIR__));
return [
    'controllerNamespace' => 'ETWS\Backend\Controllers',
    'defaultRoute' => 'dashboard',
    'viewPath' => dirname(__DIR__) . '/views',
    'HomeUrl' => '/admin/dashboard/index',
    'components' => [
//        'user' => [
//            'identityClass' => 'EtYii\backend\models\user\User',
//            'loginUrl' => ['auth/login'],
//            'enableAutoLogin' => false,
//        ],
//        'urlManager' => [
//            /** @see \EtYii\backend\UrlManager */
//            'class' => 'EtYii\backend\UrlManager',
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
//            'rules' => [
//                '/login' => '/auth/login',
//                '/logout' => '/auth/logout'
//            ],
//        ],
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],
//        'assetManager' => [
//            'linkAssets' => true,
//        ],
    ]
];