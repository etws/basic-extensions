<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 19:54
 */

namespace ETWS\Backend\Controllers;

use Yii;
use yii\filters\AccessControl;

class DashboardController extends \EtYii\backend\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}