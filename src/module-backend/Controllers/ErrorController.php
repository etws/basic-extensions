<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 19:54
 */

namespace EtYii\backend\controllers;

use Yii;
use yii\filters\AccessControl;

class ErrorController extends \EtYii\backend\Controller
{
    public $layout = 'error';
    /**
     * @inheritdoc
     */

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => \yii\web\ErrorAction::className(),
            ],
        ];
    }
}