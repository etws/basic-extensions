<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 19:54
 */

namespace EtYii\backend\controllers;

use Yii;
use EtYii\backend\models\user\forms\LoginForm;
use yii\filters\AccessControl;

class AuthController extends \EtYii\backend\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();

        if (Yii::$app->request->isPost) {
            $status = false;
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $status = true;
            }

            if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $response = [
                    'status' => $status,
                    'href' => Yii::$app->getHomeUrl()
                ];
                return $response;
            }

            return $this->goHome();
        }

        return $this->render('login', [
           'model' => $model,
        ]);
    }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}