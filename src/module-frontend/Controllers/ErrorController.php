<?php
/**
 * Created by PhpStorm.
 * User: NOX
 * Date: 14.10.2016
 * Time: 19:54
 */

namespace ETWS\Frontend\Controllers;

use ETWS\Frontend\Controller;

class ErrorController extends Controller
{
    /**
     * @inheritdoc
     */

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors();
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => \yii\web\ErrorAction::className(),
            ],
        ];
    }
}