<?php
return [
    'user' => [
        'identityClass' => 'EtYii\backend\models\user\User',
        'loginUrl' => ['auth/login'],
        'enableAutoLogin' => false,
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'enableStrictParsing' => false,
        'rules' => [
        ],
    ],
    'errorHandler' => [
        'errorAction' => 'error/index',
    ],
    'assetManager' => [
        'linkAssets' => true,
    ],
];