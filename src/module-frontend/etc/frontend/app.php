<?php
//@app/views надо принудиьельно назначать потому что иначе не работают темы, тоесть при сопоставлении путей он ядро непонимает что брать.
Yii::setAlias('@app/views', dirname(dirname(__DIR__)) . '/views');
\Yii::setAlias('@ETWS/Frontend', dirname(dirname(__DIR__)));
return [
    'controllerNamespace' => 'ETWS\Frontend\Controllers',
//    'defaultRoute' => 'dashboard',
    'viewPath' => dirname(dirname(__DIR__)) . '/views',
//    'HomeUrl' => '/admin/dashboard/index',
];