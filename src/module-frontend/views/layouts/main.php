<?php

/* @var $this \yii\web\View */
/* @var $content string */

use EtYii\backend\assets\BackendAssets;
use yii\helpers\Html;
use yii\helpers\Url;


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="leftMenu nav-collapse">
<?php $this->beginBody() ?>
<div id="wrapper">
    <div id="header">
        <div class="logo-area clearfix">
            <a href="#" class="logo"></a>
        </div>
        <div class="tools-bar">
            <ul class="nav navbar-nav nav-main-xs">
                <li><a href="#" class="icon-toolsbar nav-mini"><i class="fa fa-bars"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right tooltip-area">
                <?php /*
                <li>
                    <a class="btn" data-toggle="modal" href="#md-notification" title="Notification">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge bg-danger">2</span>
                    </a>
                </li>
                */ ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                        <em><strong>Hi</strong>, <?= Yii::$app->user->getIdentity()->username; ?> </em> <i class="dropdown-icon fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right icon-right arrow">
                        <li><a href="<?= Url::to(['/profile/edit']) ?>"><i class="fa fa-user"></i> Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="<?= Url::to(['/auth/logout']) ?>"><i class="fa fa-sign-out"></i> Signout </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="main">
        <ol class="breadcrumb">
            <li><a href="<?= Url::to(['/']) ?>">Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
        <div id="content">
            <?= $content ?>
        </div>
    </div>
    <?php /*
    <div id="md-notification" class="modal fade md-stickTop bg-danger" tabindex="-1" data-width="500">
        <div class="modal-header bd-danger-darken">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
            <h4 class="modal-title"><i class="fa fa-bell-o"></i> Notification</h4>
        </div>
        <div class="modal-body" style="padding:0">
            <div class="widget-im notification">
                <ul>
                    <li>
                        <section class="thumbnail-in">
                            <div class="widget-im-tools tooltip-area pull-right">
																<span>
																		<time class="timeago lasted" datetime="2014">when you opened the page</time>
																</span>
                                <span>
																		<a href="javascript:void(0)" class="im-action" data-toggle="tooltip" data-placement="left" title="Action"><i class="fa fa-keyboard-o"></i></a>
																</span>
                            </div>
                            <h4>Your request approved</h4>
                            <div class="im-thumbnail bg-theme-inverse"><i class="fa fa-check"></i></div>
                            <div class="pre-text">One Button (click to remove this)</div>
                        </section>
                        <div class="im-confirm-group">
                            <div class=" btn-group btn-group-justified">
                                <a class="btn btn-inverse im-confirm" href="javascript:void(0)" data-confirm="accept">Accept.</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <section class="thumbnail-in">
                            <div class="widget-im-tools tooltip-area pull-right">
																<span>
																		<time class="timeago" datetime="2013-11-17T14:24:17Z">timeago</time>
																</span>
                                <span>
																		<a href="javascript:void(0)" class="im-action" data-toggle="tooltip" data-placement="left" title="Action"><i class="fa fa-keyboard-o"></i></a>
																</span>
                            </div>
                            <h4>Dashboard new design!! you want to see now ? </h4>
                            <div class="im-thumbnail bg-theme"><i class="fa fa-bell-o"></i></div>
                            <div class="pre-text">Two Button (with link and click to close this) Lorem ipsum dolor sit amet, consectetur adipisicing elit, </div>
                        </section>
                        <div class="im-confirm-group">
                            <div class=" btn-group btn-group-justified">
                                <a class="btn btn-inverse" href="dashboard.html">Go Now.</a>
                                <a class="btn btn-theme im-confirm" href="javascript:void(0)" data-confirm="no">Later.</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    */ ?>
    <nav id="menu"  data-search="close">
        <ul>
            <li><a href="<?echo Url::to(['/dashboard/index']) ?>"><i class="icon  fa fa-laptop"></i> Dashboard</a></li>
            <li><span><i class="icon  fa fa-cog"></i> System</span>
                <ul>
                    <li class="Label label-lg">Permissions</li>
                    <li><a href="<?= Url::to(['/user/index']) ?>"> Users </a></li>
                    <li><a href="<?= Url::to(['/roles/index']) ?>"> Roles </a></li>
                    <li class="Label label-lg">Settings</li>
                    <li><a href="<?= Url::to(['/system-config/index']) ?>"> Configuration</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>