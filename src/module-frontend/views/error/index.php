<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="row align-lg-center">
    <div class="col-md-12">
        <div class="error-template">
            <h1> ERROR <?= preg_replace('/^.*? /m', '<strong>\0</strong>', $name );?> </h1>
            <h2><?= nl2br(Html::encode($message)) ?></h2>
            <div class="error-details">
                <?php // echo str_replace('#', '<br>#', $exception->getTraceAsString()); ?>
            </div>
            <div class="error-actions">
                <a href="#" class="btn btn-theme-inverse btn-lg"><span class="fa fa-envelope-o"></span>
                    Contact Support </a>
            </div>
            <div class="error-details"><a href="#" class="footer-link">&copy; 2011-<?= date('Y'); ?> ET Web
                    Solutions. All Rights Reserved.</a></div>
        </div>
    </div>
</div>